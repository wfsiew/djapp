from django.http import HttpResponse
import jsonpickle

class MyJsonResponse(HttpResponse):
    
    def __init__(self, content={}, status=None,
             content_type='application/json'):
        super(MyJsonResponse, self).__init__(jsonpickle.encode(content, unpicklable=False),
                                             status=status, content_type=content_type)