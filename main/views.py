from django.shortcuts import render, redirect
from djapp.utils import MyJsonResponse
from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from StringIO import StringIO
from django.db import transaction
from django.conf import settings
from os import listdir
from os.path import isfile, join
from docx import Document
import logging, models, os, requests

# oauth2 guide
# https://www.themarketingtechnologist.co/google-oauth-2-enable-your-application-to-access-data-from-a-google-user/

import socket, select

# Create your views here.

logger = logging.getLogger(__name__)

def index(request):
    logger.debug("hello")
    l = [1, 2, 3]
    request.session['mydata'] = l
    #request.session.clear()
    #request.session.modified = True
        
    return HttpResponse('hello')

def oauth(req):
    q = ('https://accounts.google.com/o/oauth2/auth'
        '?response_type=code'
        '&client_id=930864615622-i07iau0ghff41h3j7o0vl7c8f5l96lnc.apps.googleusercontent.com'
        '&redirect_uri=http://localhost:8000/main/callback/'
        '&scope=openid%20profile%20email'
        '&login_hint=email'
        '&hd=redtone.com'
        '&approval_prompt=force')
    s = '<a href="{0}">Google</a>'.format(q)
    return HttpResponse(s)

def callback(req):
    code = req.GET.get('code')
    url = 'https://accounts.google.com/o/oauth2/token'
    k = {
        'code': code,
        'client_id': '930864615622-i07iau0ghff41h3j7o0vl7c8f5l96lnc.apps.googleusercontent.com',
        'client_secret': 'Ley_jbug2JL36fwRJAKWYYF4',
        'redirect_uri': 'http://localhost:8000/main/callback/',
        'grant_type': 'authorization_code'
    }
    r = requests.post(url, data=k)
    d = r.json()
    print d
    
    url = 'https://www.googleapis.com/oauth2/v1/userinfo'
    k = {
        'access_token': d.get('access_token')
    }
    r = requests.get(url, params=k)
    m = r.json()
    return JsonResponse(m)

def auth(request):
    user = authenticate(username='david', password='master')
    if user is not None:
        login(request, user)
        return redirect('/main/')
    
    return HttpResponse('invalid login')

def logoff(request):
    logout(request)
    return HttpResponse('logout success')

@login_required
def data(request):
    logger.info('data')
    d = models.Person()
    dic = {
           'success': 1,
           'data': d
           }
    return MyJsonResponse(dic)

def page(request):
    d = [1, 2, 3]
    ctx = { 'data': d }
    return render(request, 'main/page.html', ctx)

def badreq(request):
    raise HttpResponseForbidden()

def recv(request):
    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    soc.setblocking(0)
    soc.bind(('localhost', 8200))
    re, se, er = select.select([soc], [], [])
    if re:
        x = soc.recv(4096)
        return HttpResponse(x)
    
    else:
        return HttpResponse('no data')
    
@transaction.atomic
def save(req):
    o = models.RequestForm()
    o.reqfrom = 1
    o.reqto = 2
    o.save()
    
    return HttpResponse('success')

def uploadpage(req):
    return render(req, 'main/uploadpage.html')

def uploadfile(req):
    if req.method == 'POST':
        lf = req.FILES.getlist('file')
        if lf is None:
            return redirect('uploadfilelist')
        
        handle_uploaded_file(lf)
        return redirect('uploadfilelist')
        
    return HttpResponseForbidden()

def uploadfilelist(req):
    dir = settings.UPLOAD_PATH
    userdir = join(dir, '__0001__')
    if not os.path.exists(userdir):
        filelist = []
        
    else:
        filelist = [f for f in listdir(userdir) if isfile(join(userdir, f))]
        
    ctx = {
           'list': filelist
           }
    return render(req, 'main/uploadfilelist.html', context=ctx)

def docfile(req):
    doc = Document()
    pa = doc.add_paragraph('hello')
    doc.add_heading('The REAL meaning of the universe')

    k = StringIO()
    doc.save(k)
    b = k.getvalue()
    k.close()
    
    return sendfile(b, 'docfile.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    
def sendfile(b, filename, content_type):
    r = HttpResponse(b, content_type=content_type)
    r['Content-Disposition'] = 'attachment; filename="{0}"'.format(filename)
    return r

def handle_uploaded_file(lf):
    dir = settings.UPLOAD_PATH
    for f in lf:
        userdir = join(dir, '__0001__')
        ensure_dir(userdir)
        
        filename = join(userdir, f.name)
        with open(filename, 'wb+') as dest:
            for chunk in f.chunks():
                dest.write(chunk)
    
def ensure_dir(f):
    if not os.path.exists(f):
        os.makedirs(f)
    