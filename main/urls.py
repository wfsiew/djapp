'''
Created on Dec 14, 2015

@author: wfsiew
'''
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^auth/$', views.auth, name='auth'),
    url(r'^logoff/$', views.logoff, name='logoff'),
    url(r'^data/$', views.data, name='data'),
    url(r'^page/$', views.page, name='page'),
    url(r'^badreq/$', views.badreq, name='badreq'),
    url(r'^recv/$', views.recv, name='recv'),
    url(r'^save/$', views.save, name='save'),
    url(r'^upload/$', views.uploadpage, name='uploadpage'),
    url(r'^upload/file/$', views.uploadfile, name='uploadfile'),
    url(r'^upload/file/list/$', views.uploadfilelist, name='uploadfilelist'),
    url(r'^docfile/$', views.docfile, name='docfile'),
]