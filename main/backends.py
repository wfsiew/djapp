#from django.contrib.auth.models import User
from main import models

#http://blackglasses.me/2013/09/17/custom-django-user-model/
#http://blog.mathandpencil.com/replacing-django-custom-user-models-in-an-existing-application/

class ClientAuthBackend(object):
    
    def authenticate(self, username=None, password=None):
        if username == 'david' and password == 'master':
            try:
                user = models.User.objects.get(username=username)
        
            except models.User.DoesNotExist:
                user = models.User(username=username, password='password')
                user.save()
                
            return user
            
        return None
        
    def get_user(self, user_id):
        try:
            return models.User.objects.get(pk=user_id)
        
        except models.User.DoesNotExist:
            return None
        