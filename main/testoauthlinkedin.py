import uuid

q = ('https://www.linkedin.com/uas/oauth2/authorization'
     '?response_type=code'
     '&client_id=756bpc1njs21xy'
     '&redirect_uri=http://localhost:8000/main/linkedin/callback/'
     '&state=' + str(uuid.uuid4()))

print q

import requests

url = 'https://www.linkedin.com/uas/oauth2/accessToken'
k = {
    'code': 'AQR6T6f0pcfW9sZ_Dx9CgvffDlqr5Ek1zMd1WvhIWAEpn7kna9bU-4GJCkp4t_pMeLSmYMIqHF8zp62N6Tc6mNEO_pRL8MCNkRbng1G_xGtZ97HanoM',
    'client_id': '756bpc1njs21xy',
    'client_secret': 'jNqljpC3PnLA7wCP',
    'redirect_uri': 'http://localhost:8000/main/linkedin/callback/',
    'grant_type': 'authorization_code'
}
r = requests.post(url, data=k)
d = r.json()
print d

# {"access_token":"AQWr2B3CVb-baPS88jxAivsfmNwKy_wgVD8O2VjLsAFzPfX_pEQePL876TM17oybmakVup7_IFIG3BF0YR898pdumqpFtd-9X7OzGCxIItn4mKaC-OObm7A6cS0gwXu6j6Wj_uPOdJgEYIZcdW5l0lvP3ApbSjgGd5gzG0k0VaGJAZIxN7g","expires_in":5183999}

headers = {
    'Authorization': 'Bearer {0}'.format(d['access_token'])
}
rx = requests.get('https://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,picture-url)', headers=headers)
print rx.text

# <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
# <person>
#   <id>wkoscbaEKF</id>
#   <email-address>siewwingfei@hotmail.com</email-address>
#   <first-name>Siew</first-name>
#   <last-name>Wing Fei</last-name>
#   <picture-url>https://media.licdn.com/mpr/mprx/0_C-W9UUkDHGcdxSe2hvsLU4-0okL4xHd281gWU4GxzTAd8uOuaqMVzZnlXo56pfHh_AwHNxpW30dc</picture-url>
# </person>