q = ('https://accounts.google.com/o/oauth2/auth'
	'?response_type=code'
	'&client_id=767995890535.apps.googleusercontent.com'
	'&redirect_uri=http://localhost/webmvc/callback'
	'&scope=openid%20profile%20email'
	'&login_hint=email')

print q

import requests

url = 'https://www.googleapis.com/oauth2/v3/token'
k = {
	'code': '4/9elpNqTOlPzJqgLklYk7vi89PIV8zYiC-VeBwyAxJWY',
	'client_id': '767995890535.apps.googleusercontent.com',
	'client_secret': 'MTnxddiDsRnuLJY6Rp9uCoo7',
	'redirect_uri': 'http://localhost/webmvc/callback',
	'grant_type': 'authorization_code'
}
r = requests.post(url, data=k)
d = r.json()
print d

rx = requests.get('https://www.googleapis.com/plus/v1/people/me', params={'access_token': d['access_token']})
print rx.json()

# {
#  "access_token": "ya29.CjXgAhFRBg2eoATtSZRBhfODif-guemI2RInL2ep8a2ldOODwxAtw3RViuRKuKynkz0WJ-4Nvg",
#  "token_type": "Bearer",
#  "expires_in": 3551,
#  "id_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjcxZjEyZGE2OTdkMDgzZGQ4MTliZDdhNGUxN2YzNjE1NTJkZDc5ZjgifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXRfaGFzaCI6Ijg3NEhmQ25URHdGX1ZCM1g1MmV0SEEiLCJhdWQiOiI3Njc5OTU4OTA1MzUuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDA3MTUxNzA2NDAyNzYzODM3OTQiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXpwIjoiNzY3OTk1ODkwNTM1LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiaGQiOiJyZWR0b25lLmNvbSIsImVtYWlsIjoid2luZ2ZlaS5zaWV3QHJlZHRvbmUuY29tIiwiaWF0IjoxNDYzMDU5MDEyLCJleHAiOjE0NjMwNjI2MTJ9.FXszMjzQgZaqNc_uf8wnZkv1kR8iroLl7wE8Wn03GsCZ6JXUFNjWMtXmoQgTHOGxgKtdkBG-V8uStTT9AfHuRRUljWurmTSeoXUgnmYB6Afvkq8oR73dUk9VhOFrjtT1twmiyknXdLWc9IOBlOEUevtSr-fp7INZoqWqjXebENYWpXEY46vhrd3gLLijrGSqwg1cu9oprDPd-ps_umc-y-zifY6DqMSUUGrQ3Fiqie1n42o70frLZJ58GUTUmLHCYDKVfiVFVBfpMcaPCmMcgCpCmQObXGM18UkagV6X0KKnaqHK2tIQqCQ4fVFc5EO9XW9GD3JxlunbIDc4WGAsbQ"
# }

# {
#  "kind": "plus#person",
#  "etag": "\"PdVrgyg49MXU0yk25IMSkjLLrEk/2Gg9RDGTSbBO8xWYwwg3EvfiRf8\"",
#  "gender": "male",
#  "emails": [
#   {
#    "value": "wingfei.siew@redtone.com",
#    "type": "account"
#   }
#  ],
#  "objectType": "person",
#  "id": "100715170640276383794",
#  "displayName": "Siew Wing Fei",
#  "name": {
#   "familyName": "Fei",
#   "givenName": "Siew Wing"
#  },
#  "url": "https://plus.google.com/100715170640276383794",
#  "image": {
#   "url": "https://lh4.googleusercontent.com/-eSwBA6XlPoY/AAAAAAAAAAI/AAAAAAAAAF8/DW4bV8eam8Q/photo.jpg?sz=50",
#   "isDefault": false
#  },
#  "isPlusUser": true,
#  "language": "en",
#  "circledByCount": 0,
#  "verified": false,
#  "domain": "redtone.com"
# }