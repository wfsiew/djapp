key = 'iJQGXxO7CsFBIElUhXappxPYv'
sec = 'JdsifIuJ47yUEzCitzpNjcVYO8n6ezS8BEh0456prCiIvZ0EJU'

from flask_oauth import OAuth

oauth = OAuth()
twitter = oauth.remote_app('twitter',
    base_url='https://api.twitter.com/1/',
    request_token_url='https://api.twitter.com/oauth/request_token',
    access_token_url='https://api.twitter.com/oauth/access_token',
    authorize_url='https://api.twitter.com/oauth/authenticate',
    consumer_key=key,
    consumer_secret=sec
)
k = twitter.authorize(callback='http://localhost:8000/main/twitter/callback/')
print k